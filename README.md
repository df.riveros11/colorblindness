# ColorBlindness


## Se implementaron las siguientes funcionalidades:
1. Poder tomar una fotografía y mostrarla en el app
2. Poder aplicar filtros para las siguientes variables de daltonismo:
    * Protanopia
    * Prontanomaly
    * Deuteranopia
    * Deuteranomaly
    * Trintanopia
    * Trinanomaly
    * Achromatopsia
    * Achromatomaly
3. Poner la imagen en default al oprimir Filtro Normal
