package com.example.colorblindness;

import android.graphics.Color;

public enum ColorBlindnessType {
	
	
	NORMAL, PROTANOPIA(RGBValue.PROTANOPIA_RGB), PRONTANOMALY(RGBValue.PRONTANOMALY_RGB),
	DEUTERANOPIA(RGBValue.DEUTERANOPIA_RGB), DEUTERANOMALY(RGBValue.DEUTERANOMALY_RGB),
	TRITANOPIA(RGBValue.TRITANOPIA_RGB), TRINANOMALY(RGBValue.TRINANOMALY_RGB),
	ACHROMATOPSIA(RGBValue.ACHROMATOPSIA_RGB), ACHROMATOMALY(RGBValue.ACHROMATOMALY_RGB);
	
	private final RGBValue rgb;
	
	ColorBlindnessType() {
		this(RGBValue.NORMAL_RGB);
	}
	
	
	ColorBlindnessType(RGBValue rgb) {
		this.rgb = rgb;
	}

	int changeImage(int color) {
		return rgb.changeImage(color);
	}
	
	private enum RGBValue {
		/**Normal:{ R:[100, 0, 0], G:[0, 100, 0], B:[0, 0, 100]}*/
		NORMAL_RGB{
			
			public final double red1 = 100.0/100;
			private final double red2 = 0.0/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 0.0/100;
			private final double green2 = 100.0/100;
			private final double green3 = 0.0/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 0.0/100;
			private final double blue3 = 100.0/100;

			@Override
			int changeRGB(int color) {
				int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				return Color.rgb(red, green, blue);
			}
			
		}, 
		/**Protanopia:{ R:[56.667, 43.333, 0], G:[55.833, 44.167, 0], B:[0, 24.167, 75.833]}*/
		PROTANOPIA_RGB {
			private final double red1 = 56.667/100;
			private final double red2 = 43.333/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 55.833/100;
			private final double green2 = 44.167/100;
			private final double green3 = 0.0/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 24.167/100;
			private final double blue3 = 75.833/100;

			@Override
			int changeRGB(int color) {
				int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				return Color.rgb(red, green, blue);
			}
			
		},
		/**Protanomaly:{ R:[81.667, 18.333, 0], G:[33.333, 66.667, 0], B:[0, 12.5, 87.5]}*/
		PRONTANOMALY_RGB{
			private final double red1 = 81.667/100;
			private final double red2 = 18.333/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 33.333/100;
			private final double green2 = 66.667/100;
			private final double green3 = 0.0/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 12.5/100;
			private final double blue3 = 87.5/100;

			@Override
			int changeRGB(int color) {
				int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				return Color.rgb(red, green, blue);
			}
			
		},
		/**Deuteranopia:{ R:[62.5, 37.5, 0], G:[70, 30, 0], B:[0, 30, 70]}*/
		DEUTERANOPIA_RGB{
			private final double red1 = 62.5/100;
			private final double red2 = 37.5/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 70/100;
			private final double green2 = 30/100;
			private final double green3 = 0.0/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 30.0/100;
			private final double blue3 = 70.0/100;

			@Override
			int changeRGB(int color) {
				int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				return Color.rgb(red, green, blue);
			}
			
		},
		/**Deuteranomaly:{ R:[80, 20, 0], G:[25.833, 74.167, 0], B:[0, 14.167, 85.833]}*/
		DEUTERANOMALY_RGB{
			private final double red1 = 80.0/100;
			private final double red2 = 20.0/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 25.833/100;
			private final double green2 = 74.167/100;
			private final double green3 = 0.0/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 14.167/100;
			private final double blue3 = 85.833/100;

			@Override
			int changeRGB(int color) {
				int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				return Color.rgb(red, green, blue);
			}
		}, 
		 /**Tritanopia:{ R:[95, 5, 0], G:[0, 43.333, 56.667], B:[0, 47.5, 52.5]}*/
		TRITANOPIA_RGB{
			private final double red1 = 95.0/100;
			private final double red2 = 5.0/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 0.0/100;
			private final double green2 = 43.333/100;
			private final double green3 = 56.667/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 47.5/100;
			private final double blue3 = 52.5/100;

			 @Override
			 int changeRGB(int color) {
				 int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				 int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				 int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				 return Color.rgb(red, green, blue);
			 }
		},
		 /**Tritanomaly:{ R:[96.667, 3.333, 0], G:[0, 73.333, 26.667], B:[0, 18.333, 81.667]}*/
		TRINANOMALY_RGB{
			private final double red1 = 96.667/100;
			private final double red2 = 3.333/100;
			private final double red3 = 0.0/100;
			
			private final double green1 = 0.0/100;
			private final double green2 = 73.333/100;
			private final double green3 = 26.667/100;
			
			private final double blue1 = 0.0/100;
			private final double blue2 = 18.333/100;
			private final double blue3 = 81.667/100;

			 @Override
			 int changeRGB(int color) {
				 int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				 int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				 int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				 return Color.rgb(red, green, blue);
			 }
		},
		 /**Achromatopsia:{ R:[29.9, 58.7, 11.4], G:[29.9, 58.7, 11.4], B:[29.9, 58.7, 11.4]}*/
		ACHROMATOPSIA_RGB{
			private final double red1 = 29.9/100;
			private final double red2 = 58.7/100;
			private final double red3 = 11.4/100;
			
			private final double green1 = 29.9/100;
			private final double green2 = 58.7/100;
			private final double green3 = 11.4/100;
			
			private final double blue1 = 29.9/100;
			private final double blue2 = 58.7/100;
			private final double blue3 = 11.4/100;

			 @Override
			 int changeRGB(int color) {
				 int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				 int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				 int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				 return Color.rgb(red, green, blue);
			 }
		},
		 /**Achromatomaly:{ R:[61.8, 32, 6.2], G:[16.3, 77.5, 6.2], B:[16.3, 32.0, 51.6]} **/
		ACHROMATOMALY_RGB{
			private final double red1 = 61.8/100;
			private final double red2 = 32.0/100;
			private final double red3 = 6.2/100;
			
			private final double green1 = 16.3/100;
			private final double green2 = 77.5/100;
			private final double green3 = 6.2/100;
			
			private final double blue1 = 16.3/100;
			private final double blue2 = 32.0/100;
			private final double blue3 = 51.6/100;

			 @Override
			 int changeRGB(int color) {
				 int red = (int) (Color.red(color)*red1 + Color.green(color)*red2 + Color.blue(color)*red3);
				 int green = (int) (Color.red(color)*green1 + Color.green(color)*green2 + Color.blue(color)*green3);
				 int blue = (int) (Color.red(color)*blue1 + Color.green(color)*blue2 + Color.blue(color)*blue3);
				 return Color.rgb(red, green, blue);
			 }
		};
		
		abstract int changeRGB(int color);
		
		int changeImage(int color) {
			return changeRGB(color);
		}
	}
		
}
