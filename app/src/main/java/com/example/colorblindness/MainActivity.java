package com.example.colorblindness;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private Button mCamera, mFiltroProtanopia, mFiltroProntanomaly, mFiltroDeuteranopia, mNormal;
    private Button mDeuteranomaly, mTritanopia, mTrinanomaly, mAchromatopsia, mAchromatomaly;
    private ImageView mView;
    private Bitmap originalImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        originalImage = null;
        mView = findViewById(R.id.imageView);
        mCamera = findViewById(R.id.camera);
        mCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 0);
            }
        });

        mFiltroProtanopia = findViewById(R.id.filtroProtanopia);
        mFiltroProtanopia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.PROTANOPIA);
            }
        });

        mFiltroProntanomaly = findViewById(R.id.filtroProntanomaly);
        mFiltroProntanomaly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.PRONTANOMALY);
            }
        });

        mFiltroDeuteranopia = findViewById(R.id.filtroDeuteranopia);
        mFiltroDeuteranopia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.DEUTERANOPIA);
            }
        });

        mNormal = findViewById(R.id.filtroNormal);
        mNormal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.setImageBitmap(originalImage);
            }
        });

        mDeuteranomaly = findViewById(R.id.filtroDeuteranomaly);
        mDeuteranomaly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.DEUTERANOMALY);
            }
        });

        mTritanopia = findViewById(R.id.filtroTrintanopia);
        mTritanopia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.TRITANOPIA);
            }
        });

        mTrinanomaly = findViewById(R.id.filtroTrinanomaly);
        mTrinanomaly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.TRINANOMALY);
            }
        });

        mAchromatopsia = findViewById(R.id.filtroAchromatopsia);
        mAchromatopsia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.ACHROMATOPSIA);
            }
        });

        mAchromatomaly = findViewById(R.id.filtroAchromatomaly);
        mAchromatomaly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap filterImage = originalImage.copy(originalImage.getConfig(), true);
                filtrateImage(filterImage, ColorBlindnessType.ACHROMATOMALY);
            }
        });
    }

    private void filtrateImage(Bitmap filterImage, ColorBlindnessType type) {
        int rgb, newRGB;
        for(int x = 0; x < filterImage.getWidth(); x++){
            for (int y = 0; y < filterImage.getHeight(); y++){
                rgb = filterImage.getPixel(x, y);
                newRGB = type.changeImage(rgb);
                filterImage.setPixel(x, y, newRGB);
            }
        }
        mView.setImageBitmap(filterImage);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = (Bitmap) data.getExtras().get("data");
        originalImage = bitmap.copy(bitmap.getConfig(), false);
        mView.setImageBitmap(bitmap);
    }
}
